# F7_ModbusRTU

#### 介绍
用于存放freemodbus移植到STM32F7平台的代码

#### 软件架构
该项目仅使用了一个串口，一个定时器（串口2，定时器4）


#### 使用教程

1.  下载代码库；
2.  将代码库解压后放在纯英文目录下（否则会导致uvprojx项目打不开）；
3.  在确保电脑安装完成Keil后打开F7_ModbusRTU-->MDK_ARM-->F7_ModbusRTU.uvprojx；
4.  打开工程项目后按F7编译，提示"F7_ModbusRTU\F7_ModbusRTU.axf" - 0 Error(s), 0 Warning(s).即可；
5.  如果使用ST-Link下载程序则连接好ST-link和直接按下F8下载即可，用串口下载的选择F7_ModbusRTU-->MDK_ARM-->F7_ModbusRTU-->F7_ModbusRTU.hex文件下载即可。

#### 实验现象及观察方法
详见[基于STM32CubeMX移植freeModbusRTU（从站）](https://blog.csdn.net/ASWaterbenben/article/details/105549750)

#### 参与贡献

1. ASWaterbenben


